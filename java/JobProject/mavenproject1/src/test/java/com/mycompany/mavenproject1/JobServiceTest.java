/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1;

import java.time.LocalDate;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Windows10
 */
public class JobServiceTest {
    
    public JobServiceTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of checkEnableTime method, of class JobService.
     */
    @Test
    public void testCheckEnableTimeTodayIsBetweenStartTimeAndEndTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021, 1, 1);
        LocalDate endTime = LocalDate.of(2021, 2, 28);
        LocalDate today = LocalDate.of(2021, 2, 24);;
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        // Assert
        assertEquals(expResult, result);
    }
    @Test
    public void testCheckEnableTimeTodayIsBeforeStartTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021, 1, 1);
        LocalDate endTime = LocalDate.of(2021, 2, 28);
        LocalDate today = LocalDate.of(2020 ,12, 31);;
        boolean expResult = false;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        // Assert
        assertEquals(expResult, result);
    }
    @Test
    public void testCheckEnableTimeTodayIsAfterEndTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021, 1, 1);
        LocalDate endTime = LocalDate.of(2021, 2, 28);
        LocalDate today = LocalDate.of(2021, 3, 1);
        boolean expResult = false;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        // Assert
        assertEquals(expResult, result);
    }
    @Test
    public void testCheckEnableTimeTodayIsEqualStartTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021, 1, 1);
        LocalDate endTime = LocalDate.of(2021, 2, 28);
        LocalDate today = LocalDate.of(2021, 1, 1);
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        // Assert
        assertEquals(expResult, result);
    }
    @Test
    public void testCheckEnableTimeTodayIsEqualEndTime() {
        System.out.println("checkEnableTime");
        // Arrange
        LocalDate startTime = LocalDate.of(2021, 1, 1);
        LocalDate endTime = LocalDate.of(2021, 2, 28);
        LocalDate today = LocalDate.of(2021, 2, 28);
        boolean expResult = true;
        // Act
        boolean result = JobService.checkEnableTime(startTime, endTime, today);
        // Assert
        assertEquals(expResult, result);
    }
    
}
